package ru.t1.stepanishchev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@NotNull String userId, @NotNull String sort);

    @Nullable
    Task findOneById(@NotNull String userId, @NotNull String id);

    void clear();

    void clear(@NotNull String userId);

    @NotNull
    List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId);

}