package ru.t1.stepanishchev.tm.service.dto;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.stepanishchev.tm.api.service.IConnectionService;
import ru.t1.stepanishchev.tm.api.service.dto.ITaskDTOService;
import ru.t1.stepanishchev.tm.dto.model.TaskDTO;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.enumerated.TaskSort;
import ru.t1.stepanishchev.tm.exception.entity.StatusEmptyException;
import ru.t1.stepanishchev.tm.exception.entity.TaskNotFoundException;
import ru.t1.stepanishchev.tm.exception.field.IdEmptyException;
import ru.t1.stepanishchev.tm.exception.field.NameEmptyException;
import ru.t1.stepanishchev.tm.exception.field.ProjectIdEmptyException;
import ru.t1.stepanishchev.tm.exception.field.UserIdEmptyException;
import ru.t1.stepanishchev.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.Collection;
import java.util.List;

public final class TaskDTOService extends AbstractUserOwnedDTOService<TaskDTO, ITaskDTORepository>
        implements ITaskDTOService {

    public TaskDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public ITaskDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskDTORepository(entityManager);
    }

    @Override
    @NotNull
    public String getSortType(@Nullable final TaskSort sort) {
        if (sort == TaskSort.BY_CREATED) return "created";
        if (sort == TaskSort.BY_STATUS) return "status";
        else return "name";
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.add(task);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO updateById(
            @Nullable String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(task);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskDTO removeOneById(
            @Nullable String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.remove(task);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(task);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.clear(userId);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.clear();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public List<TaskDTO> findAll(@Nullable String userId, @Nullable TaskSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        try {
            return repository.findAll(userId, getSortType(sort));
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        try {
            @Nullable final TaskDTO task = repository.findOneById(userId, id);
            return task;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void set(@NotNull final Collection<TaskDTO> tasks) {
        if (tasks.isEmpty()) return;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.addAll(tasks);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        try {
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

}