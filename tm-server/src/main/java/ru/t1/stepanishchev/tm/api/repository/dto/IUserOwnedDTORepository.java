package ru.t1.stepanishchev.tm.api.repository.dto;

import ru.t1.stepanishchev.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends IAbstractDTORepository<M> {

}