package ru.t1.stepanishchev.tm.api.service.dto;

import ru.t1.stepanishchev.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.stepanishchev.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IUserOwnedDTORepository<M>, IAbstractDTOService<M> {
}