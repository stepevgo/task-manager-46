package ru.t1.stepanishchev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.stepanishchev.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;

public class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

}