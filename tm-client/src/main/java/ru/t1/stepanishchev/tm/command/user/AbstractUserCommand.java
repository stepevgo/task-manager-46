package ru.t1.stepanishchev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.api.endpoint.IAuthEndpoint;
import ru.t1.stepanishchev.tm.api.endpoint.IUserEndpoint;
import ru.t1.stepanishchev.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserEndpoint getUserEndpoint() {
        return getServiceLocator().getUserEndpoint();
    }

    @NotNull
    protected IAuthEndpoint getAuthEndpoint() {
        return serviceLocator.getAuthEndpoint();
    }

}